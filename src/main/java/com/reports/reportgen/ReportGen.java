package com.reports.reportgen;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ReportGen {
    public static void main(String[] args) throws Exception {

        try {
            String settingsFileName;
            String dataFileName;
            String reportFileName;

            /*Получение имен файлов из строки аргументов.
             * Если имя файла не было указано, используется значение по умолчанию.
             */
            if (args.length <= 0) {
                settingsFileName = "settings.xml";
            } else {
                settingsFileName = args[0];
            }
            if (args.length <= 1) {
                dataFileName = "source-data.tsv";
            } else {
                dataFileName = args[1];
            }
            if (args.length <= 2) {
                reportFileName = "example-report.txt";
            } else {
                reportFileName = args[2];
            }

            //Получение объекта с настройками.
            Settings settings = new Settings(settingsFileName);

            //Проверка
            System.out.println("Ширина: " + settings.getPageParams().get(Settings.Params.WIDTH) + " " +
                    "Высота: " + settings.getPageParams().get(Settings.Params.HEIGHT));
            System.out.println("Столбцы: " + settings.getColumns());

            StringBuilder dataStr = new StringBuilder("");

            //Получение отформатированных готовых данных в виде строки.
            getDataAsString(dataFileName, dataStr, settings);

            //Вывод в файл
            ByteBuffer outputBuffer = ByteBuffer.wrap(dataStr.toString().getBytes(Charset.forName("UTF-8")));
            try (FileOutputStream fos = new FileOutputStream(reportFileName)) {
                fos.getChannel().write(outputBuffer);
            } catch (FileNotFoundException e) {
                throw new Exception("Не удалось создать файл. " + e);
            } catch (IOException e) {
                throw new Exception("Ошибка ввода-вывода " + e);
            }

            System.out.println(dataStr);
        } catch (Exception e) {
            System.out.println("Ошибка! " + e.getMessage());
            return;
        }

    }

    //Проверка расширения файла
    private static boolean checkTsv(String dataFileName) {
        Pattern tsvPattern = Pattern.compile(".+\\.(tsv)");
        return tsvPattern.matcher(dataFileName).matches();
    }

    //Открытие файла и получение и обработка содержимого
    private static StringBuilder getDataAsString(String dataFileName, StringBuilder dataStr, Settings settings)
            throws Exception {
        StringBuilder header = new StringBuilder("");
        if (checkTsv(dataFileName)) {
            try (FileInputStream fin = new FileInputStream(dataFileName)) {
                try (InputStreamReader dataIn = new InputStreamReader(fin, StandardCharsets.UTF_8)) {
                    try (Scanner dataScan = new Scanner(dataIn)) {
                        //Получение отформатированного заголовка
                        getFormatHeader(settings, header);
                        //Получение отформатированных даннных
                        dataStr.append(getFormatData(dataScan, settings, header));
                    }
                }
            } catch (FileNotFoundException e) {
                throw new Exception("Файл не найден " + e);
            } catch (IOException e) {
                throw new Exception("Ошибка ввода-вывода " + e);
            }

            if (dataStr.length() > 0) {
                return dataStr;
            } else {
                throw new Exception("Файл с данными пуст.");
            }

        } else {
            throw new Exception("Файл данных должен быть в формате .tsv");
        }
    }

    //Метод формирует отформатированную строку с извлеченными данными.
    private static StringBuilder getFormatData(Scanner dataScan, Settings settings, StringBuilder header) {
        StringBuilder currentLine = new StringBuilder("");
        StringBuilder resultStr = new StringBuilder("");
        StringBuilder newLine = new StringBuilder("");

        boolean headerTooHigh = false;
        int pageHeight = settings.getPageParams().get(Settings.Params.HEIGHT);
        int headerHeight = countLines(header);
        int pageHeightWithoutHeader = pageHeight - headerHeight;

        //Проверка высоты заголовка
        if (headerHeight >= (pageHeight - 3)) {
            System.out.println("Высота заголовка слишком большая. Заголовок будет отображен только на первой странице.");
            headerTooHigh = true;
        }

        do {
            //Перебор строк
            currentLine.append(dataScan.nextLine());
            //Получение отформатированной "линии" (строки)
            currentLineFormat(settings, currentLine, newLine, resultStr);
            //Добавление линии с "-" после каждой строки
            for (int pageWidth = 1; pageWidth <= settings.getPageParams().get(Settings.Params.WIDTH); pageWidth++){
                resultStr.append("-");
            }
            resultStr.append(System.lineSeparator());
        } while (dataScan.hasNextLine());

        //Добавление заголовка в начало.
        resultStr.insert(0, header);
        for (int pageWidth = 1; pageWidth <= settings.getPageParams().get(Settings.Params.WIDTH); pageWidth++){
            resultStr.insert(header.length(),"-");
        }
        resultStr.insert(header.length()
                + settings.getPageParams().get(Settings.Params.WIDTH)
                , System.lineSeparator()
        );

        //Подсчёт кол-ва получившихся линий
        int countResultDataLines = countLines(resultStr);
        int nextSep = 0;

        //Разбивка на страницы
        for (int i = 1; i < countResultDataLines; i++) {
            nextSep = resultStr.indexOf(System.lineSeparator(), nextSep + System.lineSeparator().length());
            if (i % pageHeight == 0) {
                //Вставка разделителя страниц и заголовка
                nextSep += System.lineSeparator().length();
                resultStr.insert(nextSep, '~');
                resultStr.insert(nextSep + 1, System.lineSeparator());
                nextSep += System.lineSeparator().length() + 1;

                //Если заголовок больше, чем "<высота страницы> - 3",
                // заголовок добавляется только в начало первой страницы.
                if (!headerTooHigh) {
                    resultStr.insert(nextSep, header);
                    nextSep += header.length();

                    for (int pageWidth = 1; pageWidth <= settings.getPageParams().get(Settings.Params.WIDTH); pageWidth++) {
                        resultStr.insert(nextSep, "-");
                        nextSep += 1;
                    }
                    resultStr.insert(nextSep, System.lineSeparator());
                    nextSep += System.lineSeparator().length();
                }
            }
        }

        //Замена табуляции на разделители
        replaceTabs(resultStr);

        return resultStr;
    }

    //Метод формирует отформатированный заголовок
    private static StringBuilder getFormatHeader(Settings settings, StringBuilder header){
        StringBuilder currentLine = new StringBuilder("");
        StringBuilder resultStr = new StringBuilder("");
        StringBuilder newLine = new StringBuilder("");

        for (String columnName : settings.getColumns().keySet()) {
            currentLine.append(columnName)
                    .append("\t");
        }
        currentLine.deleteCharAt(currentLine.lastIndexOf("\t"));

        currentLineFormat(settings, currentLine, newLine, resultStr);
        header.append(resultStr);
        resultStr.delete(0, resultStr.length());
        return header;
    }

    //Метод формирует отформатированную "линию" (строку). Результат добавляется в новый объект resultStr.
    private static StringBuilder currentLineFormat(Settings settings, StringBuilder currentLine, StringBuilder newLine, StringBuilder resultStr) {
        StringBuilder currentCol = new StringBuilder("");
        int prevTab = 0, nextTab;
        resultStr.append("| ");
        //Перебор столбцов
        for (int columnWidth : settings.getColumns().values()) {
            nextTab = currentLine.indexOf("\t", prevTab);
            //Извлечение содержимого столбца и удаление лишних пробелов с краев полученной строки.
            currentCol.append(currentLine.substring(prevTab, nextTab == -1 ? currentLine.length() : nextTab).trim());
            int length = currentCol.length();
            //Если ширина столбца из настроек меньше фактической ширины данных в столбце.
            if (columnWidth < length) {

                //Определение разделителя, по которому будет выполняться разбивка слова.
                // Поиск границы слова, если граница столбца проходит внутри него.
                String[] words = currentCol.toString().split("\\b");
                int countWords = -1;
                int wordsWidth = 0;
                //Разделитель, по которому будет выполняться перенос.
                int sep = 0;
                for (String word: words) {
                    countWords++;
                    wordsWidth += word.length();
                    if (wordsWidth > columnWidth) {
                        int q = wordsWidth - columnWidth;
                        //Если длина слова > длины столбца, разбивка посередине слова.
                        if (words[countWords].length() > columnWidth) {
                            if (wordsWidth - (words[countWords].length() / 2) <= columnWidth){
                                //Если длина половины слова влезает в текущий столбец, разделение по этой позиции.
                                sep = wordsWidth - (words[countWords].length() / 2);
                            } else {
                                //Иначе - по ширине столбца из настроек (что влезло, то влезло).
                                sep = columnWidth;
                            }
                        } else {
                            //Иначе слово целиком переносится на новую строку.
                            sep = wordsWidth - words[countWords].length();
                        }
                        break;
                    }
                }

                //Перенос остатка на новую строку
                newLine.append(currentCol.substring(sep, length));
                //Удаление из значения текущего столбца всех символов после определенного выше разделителя.
                currentCol.delete(sep, length);
                //Заполнение пустого пространства справа пробелами
                for (int q = 1; q <= columnWidth - sep; q++){
                    currentCol.append(" ");
                }
            } else {
                //Заполнение пустого пространства справа пробелами
                for (int q = 1; q <= columnWidth - length; q++){
                    currentCol.append(" ");
                }
            }
            prevTab = nextTab + 1;
            //Добавление столбца в результирующий объект
            resultStr.append(currentCol).append("\t");
            newLine.append("\t");
            currentCol.delete(0, currentCol.length());
        }
        resultStr.append(System.lineSeparator());

        currentLine.delete(0, currentLine.length());

        /*Если остаток, вынесенный на новую "линию" (строку), превышает кол-во столбцов,
        * т.е. в этом остатке есть какие-либо символы, кроме tab-ов,
        * для этого остатка рекурсивно вызывается метод формирования отформатированной линии
        * до тех пор, пока в остатке не останутся только tab-ы.
        */
        if (newLine.length() > settings.getColumns().size()) {
            //Новая линия становится текущей...
            currentLine.append(newLine);
            //и очищается.
            newLine.delete(0, newLine.length());
            //В вызываемый метод новая линия передается как "текущая".
            currentLineFormat(settings, currentLine, newLine, resultStr);
        }
        newLine.delete(0, newLine.length());

        return resultStr;
    }

    //Метод заменяет tab-ы на разделители вида " | " между столбцами и " |" в конце строки (линии).
    private static StringBuilder replaceTabs(StringBuilder resultStr){
        int prevTab = 0, nextTab = 0;
        nextTab = resultStr.indexOf("\t", prevTab);
        while (nextTab != -1) {
            if (resultStr.indexOf(System.lineSeparator(), nextTab) > nextTab + 1){
                resultStr.replace(nextTab, nextTab + 1, " | ");
            } else {
                resultStr.replace(nextTab, nextTab + 1, " |");
            }
            nextTab = resultStr.indexOf("\t", prevTab);
            prevTab = nextTab + 1;
        }
        return resultStr;
    }

    //Подсчет строк в наборе строк (который на самом деле явл. одной строкой)
    // через подсчет знаков переноса строки
    private static int countLines(StringBuilder dataSet){
        int count = 0;
        int nextSep = dataSet.indexOf(System.lineSeparator(), 0);
        while (nextSep != -1){
            nextSep = dataSet.indexOf(System.lineSeparator(), nextSep + System.lineSeparator().length());
            count++;

        }
        return count;
    }

}
