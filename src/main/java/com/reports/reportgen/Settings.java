package com.reports.reportgen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import static java.lang.Double.valueOf;

class Settings {
    protected enum Params { WIDTH, HEIGHT };
    private HashMap<Params, Integer> pageParams = new HashMap<>();
    private LinkedHashMap<String, Integer> columns = new LinkedHashMap<>();

    protected Settings(String settingsFileName) throws Exception {
        StringBuilder settingsStr = new StringBuilder("");
        //Получение настроек в виде строки.
        getSettingsAsString(settingsFileName, settingsStr);

        //Поиск тэга <settings>
        int settingsEnd = settingsStr.indexOf("<settings>");
        //Если тэг найден, выполняется извлечение параметров
        if (settingsEnd > 0) {
            pageParams.putAll(setPageParams(settingsStr, settingsEnd));
            columns.putAll(setColumns(settingsStr, settingsEnd));
            if (checkPageWidth(pageParams, columns)){
                throw new Exception("Ширина страницы меньше суммарной ширины столбцов с учетом их разделителей.");
            }
        } else {
            throw new Exception("Тэг <settings> не найден.");
        }
    }

    //Проверка расширения файла
    private boolean checkXml(String settingsFileName) {
        Pattern xmlPattern = Pattern.compile(".+\\.(xml)");
        return xmlPattern.matcher(settingsFileName).matches();
    }

    //Проверка превышения суммарной ширины столбцов с разделителями
    // ширины страницы, заданной в настройках.
    private boolean checkPageWidth (HashMap<Params, Integer> pageParams, LinkedHashMap<String, Integer> columns){
        int pageWidth = pageParams.get(Params.WIDTH);
        int columnsSum = 1; //Самый первый |
        /*Вычисление суммарной ширины столбцов и их разделителей с пробелами " | "
          Самый последний разделитель | не имеет пробела после себя и
          этот пробел "считается" как пробел после самого первого |
        */
        for (Integer columnValue:
             columns.values()) {
            columnsSum += columnValue + 3;
        }
        return pageWidth < columnsSum;
    }

    //Открытие файла и передача содержимого в объект типа StringBuffer
    private StringBuilder getSettingsAsString(String settingsFileName, StringBuilder settingsStr) throws Exception {
        if (checkXml(settingsFileName)) {
            int i;
            try (FileInputStream fin = new FileInputStream(settingsFileName)) {
                try (InputStreamReader dataIn = new InputStreamReader(fin, StandardCharsets.UTF_8)) {
                    do {
                        i = dataIn.read();
                        if (i != -1) {
                            settingsStr.append((char) i);
                        }
                    } while (i != -1);
                }
            } catch (FileNotFoundException e) {
                throw new Exception("Файл не найден " + e);
            } catch (IOException e) {
                throw new Exception("Ошибка ввода-вывода " + e);
            }

            if (settingsStr.length() > 0) {
                return settingsStr;
            } else {
                throw new Exception("Файл с настройками пуст.");
            }

        } else {
            throw new Exception("Файл настроек должен быть в формате .xml");
        }
    }

    //Парсинг содержимого файла настроек и
    // извлечение из него параметров страницы (ширины и высоты)
    private HashMap<Params, Integer> setPageParams(StringBuilder settingsStr, int settingsEnd) throws Exception {
        int width = 0, height = 0;

        //Поиск тэга <page> и <columns>
        int pageEnd = settingsStr.indexOf("<page>", settingsEnd);

        //Обработка содержимого тэга <page>
        if (pageEnd > 0) {
            //Поиск тэгов <width>, <height>; сохранение их содержимого
            int widthOpen = settingsStr.indexOf("<width>", pageEnd) + 7;
            int widthClose = settingsStr.indexOf("</width>", widthOpen);
            if (widthOpen > 0 && widthClose > 7) {
                width = valueOf(settingsStr.substring(widthOpen, widthClose).trim()).intValue();
            } else {
                throw new Exception("Не найден открывающий или закрывающий тэг <width>.");
            }

            int heightOpen = settingsStr.indexOf("<height>", pageEnd) + 8;
            int heightClose = settingsStr.indexOf("</height>", widthOpen);
            if (heightOpen > 0 && heightClose > 8) {
                height = valueOf(settingsStr.substring(heightOpen, heightClose).trim()).intValue();
            } else {
                throw new Exception("Не найден открывающий или закрывающий тэг <height>.");
            }

            //Проверка полученных значений
            if (width == 0) {
                throw new Exception("Задана пустая или нулевая ширина страницы.");
            } else {
                pageParams.put( Params.WIDTH, width);
            }
            if (height == 0) {
                throw new Exception("Задана пустая или нулевая высота страницы.");
            } else {
                pageParams.put( Params.HEIGHT, height);
            }

        } else {
            throw new Exception("Тэг <page> не найден.");
        }

        return pageParams;
    }

    //Парсинг содержимого файла настроек и
    // извлечение из него названий и ширины столбцов.
    private HashMap<String, Integer> setColumns(StringBuilder settingsStr, int settingsEnd) throws Exception {
        int columnsOpen = settingsStr.indexOf("<columns>", settingsEnd) - settingsEnd;
        int columnsEnd = settingsStr.indexOf("</columns>", columnsOpen);

        //Обработка содержимого тэга <columns>
        if (columnsOpen > 0) {
            //Получение позиции первого тэга <column>
            int columnOpen = settingsStr.indexOf("<column>", columnsOpen);
            int titleOpen, titleClose, columnWidthOp, columnWidthCl, columnWidth;
            String columnTitle;

            if (columnOpen != 0){
                for (int c = columnOpen; c < columnsEnd; c = columnOpen > 0 ? columnOpen : columnsEnd){
                    //Получение позиций конца открывающих тэгов и начала закрывающих тэгов title и width
                    titleOpen = settingsStr.indexOf("<title>", columnOpen) + 7;
                    titleClose = settingsStr.indexOf("</title>", titleOpen);
                    columnWidthOp = settingsStr.indexOf("<width>", columnOpen) + 7;
                    columnWidthCl = settingsStr.indexOf("</width>", columnWidthOp);
                    /*Получение содержимого тэгов title и width
                     *Если наименование столбца не указано, вставляется пробел.
                     *Если ширина столбца не указана, ширина считается = 0.
                     */
                    columnTitle = settingsStr.substring(titleOpen, titleClose).isEmpty() ?
                            " " :
                            settingsStr.substring(titleOpen, titleClose).trim();
                    columnWidth = settingsStr.substring(columnWidthOp, columnWidthCl).isEmpty() ?
                            0 :
                            valueOf(settingsStr.substring(columnWidthOp, columnWidthCl).trim()).intValue();

                    if (!columnTitle.isEmpty() && columnWidth > 0) {
                        /*Если в файле настроек есть несколько столбцов с одинаковыми наименованиями,
                            выполняется принудительное переименование каждого следующего такого столбца.
                         */
                        if (columns.containsKey(columnTitle)) {
                            System.out.println("В файле настроек наименование столбца " + columnTitle + " не уникально. ");
                            columnTitle = columnTitle + "_" + c;
                            System.out.println("Столбец будет принудительно переименован в " + columnTitle);
                        }
                        //Сохранение пары title-width
                        columns.put(columnTitle, columnWidth);
                    }

                    columnOpen = settingsStr.indexOf("<column>", columnOpen + 8);
                }
            } else {
                throw new Exception("Тэг <column> не найден.");
            }
        } else {
            throw new Exception("Тэг <columns> не найден.");
        }

        return columns;
    }

    protected HashMap<Params, Integer> getPageParams() {
        return pageParams;
    }

    protected LinkedHashMap<String, Integer> getColumns() {
        return columns;
    }
}
